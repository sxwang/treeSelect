### jquery 树形下拉框插件

TreeSelect,ztree开发的树形下拉选择框插件，支持ie8+,谷歌，火狐，360等浏览器<br>
现在升级2.0名字由原来的 _**MultipleTreeSelect改为TreeSelect.js**_ 
api更加丰富。结构更加清晰
#### TreeSelect有如下主要特点
- 兼容ie8以及各大主流浏览器
- 低侵入式使用
- 使用简单方便
- 继承了ztree高效渲染


**TreeSelect 欢迎使用本插件: TreeSelect**

#### 单选示例图片：
![单选](https://images.gitee.com/uploads/images/2020/0926/114219_7f16e3e6_2042758.png "T9GX9M[EB65YGW(3W[LV$F9.png")
#### 多选选示例图片
![多选](https://images.gitee.com/uploads/images/2020/0926/114303_314d260c_2042758.png "E8}@J@WHZAX8HFW)M}EELZT.png")
#### 快速上手示例

**1. 引入jquery,ztree,TreeSelect等js和css文件**

```html
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<html>
<head>
    <title>$Title$</title>
    <link type="text/css" rel="stylesheet" href="css/demo.css">
    <link type="text/css" rel="stylesheet" href="css/metroStyle/metroStyle.css">
    <script src="js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.ztree.all.js"></script>
    <script type="text/javascript" src="treeSelect.2.0.js"></script>

    <script>


    </script>
    <script type="text/javascript">

        /*节点列表（这里演示的是非异步方式的实现）*/
       var zNodes = [
                   {id: 1, pId: 0, name: "草帽团", open: true},
                   {id: 11, pId: 1, name: "索隆", open: true},
                   {id: 12, pId: 1, name: "路飞"},
                   {id: 13, pId: 1, name: "山治"},
                   {id: 14, pId: 1, name: "乌索普"},
                   {id: 15, pId: 1, name: "娜美"},
                   {id: 16, pId: 1, name: "布鲁克"},
                   {id: 16, pId: 1, name: "乔巴"},
                   {id: 2, pId: 0, name: "红心海贼团", open: true},
                   {id: 21, pId: 2, name: "罗"},
                   {id: 26, pId: 2, name: "贝波"},
                   {id: 22, pId: 2, name: "强巴鲁"}
               ];
        /*注册下拉树方法也很简单*/
        $(document).ready(function () {
            $("textarea").treeSelect({
            zNodes: zNodes
        });
        });
        //-->
    </script>
</head>

<body style= "width: 1080px ;margin: 0 auto" ><br>
<!--在此元素上进行渲染下拉树-->
<textarea   style="width: 300px" checks="1,11,2,23"  type="text" readonly></textarea>

</body>
</html>

```

**2.异步加载配置实例**

```js
 var options = {
             async: {
                 enable: true,
                 url: "http://qqxh.net"
             }
         }
```

**3.json配置项**
```js
 var options = {
             
             async: {
                 enable: true,
                 url: "异步加载url地址"
             },
             chkStyle: "radio",/*radio：单选模式(Radio mode)，checkbox：多选模式(checkbox mode)，默认为多选*/
             radioType : "all",/*all：整个树只能有一个选中，level：在每一级节点范围内当做一个分组*/
             height:433,/*容器高度默认200px*/
             callback:{
                             onCheck: function (treeSelectObj,treeNode) {/*选中事件的回调*/
             },
              /*Y和N分别表示选中事件和取消选中事件，p表示是否级联父级，s表示是否级联子级别，默认是都级联*/
             chkboxType: {"Y": "ps", "N": "ps"},
             checks: [1,2,3,4,5],/*默认选中值*/
             direction: "auto",/* up向上,down向下,auto自动适应  默认auto*/
             filter:true,/* 是否开启过滤 默认true*/
             searchShowParent: true,/* 搜索是否展示父级默认false*/
             beforeSearchPromise: function (defer, treeSelectObj) {
                         /*模拟异步加载,正常场景为ajax请求成功后加载数据到treeSelectObj后再调用defer.resolve()*/
                         setTimeout(function () {
                             defer.resolve();
                         }, 500);
                     }
         }
```
**3.dom配置项**

| 配置项    | 实例        | 描述                          |
|--------|-----------|-----------------------------|
| checks | 1,2,3,4,5 | 默认值。除了json配置项之外也可以这样设置默认选中值 |

```
 <textarea   style="width: 300px;overflow:hidden;"  checks ="1,11,2,23"   type="text" readonly>
 </textarea>

```


**4.api列表**

   | api | 参数 | 描述 |
|-----|----|----|
|   text()  |  void  | 获取选中后的文本信息   |
|   val()  |  void    |  获取选中后的key列表，逗号分隔  |
|   val([1,2,3])  |  key列表  |  重新赋值选中的key  |
|   destory()  |  void|  销毁treeSelect，销毁后不可再调用所有的的api  |
|  reloadNode()  |  空或者节点列表|  重新加载节点（异步情况是从后台获取） |


- 获取选中文本
```js
  
 
var obj=$("#yourContentId").treeSelect(options);(如果这里jquery选择器获取到多个元素,那么这里返回的是TreeSelect对象列表)

    obj.text();
 

```
### 常见问题:

| 问题       | 方案                             |
|----------|--------------------------------|
| 1.如何再次赋选中值 | 通过treeSelect对象的val([1,2,3]);方法 |
| 2.选中回调函数如何获取节点数据        |    onCheck回调函数会传给你两个参数：function(1.treeSelect对象，2.选中节点数据）{}|
| 3.如何禁止选择父节点      | node数据种添加chkDisabled为true即可例如：{ "id":1, "name":"test1", "checked":true, "chkDisabled":true}|
| 4.如何不级联选择 | 通过添加如下配置项即可实现 chkboxType: {"Y": "", "N": ""}, |

### 注意事项:


检索功能只支持前端检索，也就是说想使用异步模式的话，需要一次把树节点加载完全然后检索才有意义。配合异步后台检索暂时不考虑实现。
如果节点数量特别多的话，建议第一批先加载前n层级的节点。然后用户使用搜索功能的话，先加载后台命中节点后再执行前端检索。


TreeSelect 开发者网站:http://www.qqxh.net

